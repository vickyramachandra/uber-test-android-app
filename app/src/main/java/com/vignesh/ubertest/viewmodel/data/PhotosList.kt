package com.vignesh.ubertest.viewmodel.data

import com.vignesh.ubertest.repository.data.Photo

data class PhotosList(val photos: List<Photo>, val message: String, val error: Throwable? = null)
