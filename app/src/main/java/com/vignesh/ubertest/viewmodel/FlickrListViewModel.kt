package com.vignesh.ubertest.viewmodel

import com.vignesh.ubertest.repository.FlickrRepository
import com.vignesh.ubertest.viewmodel.data.PhotosList
import io.reactivex.Observable

class FlickrListViewModel(private val flickrRepository: FlickrRepository) {
    fun getPictures(searchTerm: String, pageNumber: Int): Observable<PhotosList> {
        return flickrRepository.getPictures(searchTerm, pageNumber).map {
            PhotosList(it, "Searched pictures")
        }.onErrorReturn {
            PhotosList(emptyList(), "An error occured", Throwable("error"))
        }
    }
}