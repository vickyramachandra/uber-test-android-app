package com.vignesh.ubertest.network

import android.content.Context
import android.graphics.Bitmap
import android.support.v4.util.LruCache
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

object NetworkManager {
    private lateinit var context: Context

    val requestQueue: RequestQueue by lazy { Volley.newRequestQueue(context) }

    val imageLoader: ImageLoader by lazy {
        val mCache = LruCache<String, Bitmap>(1000)
        ImageLoader(requestQueue, object : ImageLoader.ImageCache {
            override fun getBitmap(url: String?): Bitmap? {
                return mCache.get(url)
            }

            override fun putBitmap(url: String?, bitmap: Bitmap?) {
                if (bitmap != null) {
                    mCache.put(url, bitmap)
                }
            }
        })
    }

    fun initialize(context: Context) {
        this.context = context.applicationContext
    }

    fun getRequest(url: String, networkCallbacks: NetworkCallbacks) {
        Log.d("N/W request  ", url)
        val stringrequest = StringRequest(Request.Method.GET, url,
                Response.Listener<String> { response ->
                    networkCallbacks?.onSuccess(response)
                },
                Response.ErrorListener { error ->
                    networkCallbacks?.onError(error)
                })
        requestQueue.add(stringrequest)
    }
}