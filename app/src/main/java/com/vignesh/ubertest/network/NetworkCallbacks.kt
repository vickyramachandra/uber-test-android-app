package com.vignesh.ubertest.network

interface NetworkCallbacks {
    fun onSuccess(response: String)
    fun onError(exception: Exception)
}