package com.vignesh.ubertest.application

import android.app.Application
import com.vignesh.ubertest.repository.FlickrRepository
import com.vignesh.ubertest.repository.api.FlickrApi
import com.vignesh.ubertest.viewmodel.FlickrListViewModel

class UberChallenge : Application() {

    // Can be replaced by Dagger
    companion object {
        private lateinit var flickrApi: FlickrApi
        private lateinit var flickrRepository: FlickrRepository
        private lateinit var flickrListViewModel: FlickrListViewModel

        fun injectFlickrListViewModel() = flickrListViewModel
    }

    override fun onCreate() {
        super.onCreate()
        flickrApi = FlickrApi(this)
        flickrRepository = FlickrRepository(flickrApi)
        flickrListViewModel = FlickrListViewModel(flickrRepository)
    }
}