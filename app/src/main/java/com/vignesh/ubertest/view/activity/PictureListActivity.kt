package com.vignesh.ubertest.view.activity

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.widget.Toast
import com.vignesh.ubertest.R
import com.vignesh.ubertest.application.UberChallenge
import com.vignesh.ubertest.view.adapter.PhotoAdapter
import com.vignesh.ubertest.viewmodel.data.PhotosList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_picture_list.*
import java.net.ConnectException
import java.net.UnknownHostException

class PictureListActivity : AppCompatActivity() {

    private val subscriptions = CompositeDisposable()
    private val flickrListViewModel = UberChallenge.injectFlickrListViewModel()
    private lateinit var gridLayoutManager: GridLayoutManager

    private var searchTerm = "kittens"
    private var loading = false
    private var pageNumber = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picture_list)
        initViews()
        setScrollListener()
        setSearchQueryListener()
        subscribe(true) // by default load kittens
    }

    private fun initViews() {
        swipeRefreshLayout.isRefreshing = true
        gridLayoutManager = GridLayoutManager(this, 3)
        picturesList.layoutManager = gridLayoutManager
        picturesList.adapter = PhotoAdapter(ArrayList())
    }

    private fun setScrollListener() {
        picturesList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = gridLayoutManager.childCount
                val totalItemCount = gridLayoutManager.itemCount
                val firstVisible = gridLayoutManager.findFirstVisibleItemPosition()
                if (!loading && (visibleItemCount + firstVisible) >= totalItemCount) {
                    loading = true
                    pageNumber++
                    // Call your API to load more items
                    swipeRefreshLayout.isRefreshing = true
                    subscribe(false)
                }
            }
        })
    }

    private fun setSearchQueryListener() {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    searchTerm = query
                    loading = false
                    pageNumber = 1
                    swipeRefreshLayout.isRefreshing = true
                    subscribe(true)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    private fun subscribe(isRefresh: Boolean) {
        val disposable = flickrListViewModel.getPictures(searchTerm, pageNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    showUsers(isRefresh, it)
                    swipeRefreshLayout.isRefreshing = false
                }, {
                    showError(R.string.unknown_error)
                    swipeRefreshLayout.isRefreshing = false
                })
        subscriptions.add(disposable)
    }

    private fun showUsers(isRefresh: Boolean, data: PhotosList) {
        swipeRefreshLayout.isEnabled = false
        loading = false
        if (data.error == null) {
            if (isRefresh) {
                (picturesList.adapter as PhotoAdapter).clearList()
            }
            (picturesList.adapter as PhotoAdapter).updateDataset(data.photos)
        } else if (data.error is ConnectException || data.error is UnknownHostException) {
            showError(R.string.network_error)
        } else {
            showError(R.string.unknown_error)
        }
    }

    private fun showError(@StringRes stringresource: Int) {
        swipeRefreshLayout.isEnabled = false
        loading = false
        Snackbar.make(parentLayout, getString(stringresource), Toast.LENGTH_LONG).show()
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()
    }
}
