package com.vignesh.ubertest.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.toolbox.NetworkImageView
import com.vignesh.ubertest.R
import com.vignesh.ubertest.network.NetworkManager
import com.vignesh.ubertest.repository.data.Photo

class PhotoAdapter(private val photosList: ArrayList<Photo>) : RecyclerView.Adapter<PhotoAdapter.ViewHolder>() {

    class ViewHolder : RecyclerView.ViewHolder {
        var imageView: NetworkImageView
        constructor(itemView: View) : super(itemView) {
            imageView = itemView.findViewById(R.id.picture_view) as NetworkImageView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_picture, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val photo = photosList[position]
        holder.imageView.setImageUrl("http://farm${photo.farm}.static.flickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg", NetworkManager.imageLoader)
    }

    override fun getItemCount() = photosList.size

    fun clearList() {
        this.photosList.clear()
        notifyDataSetChanged()
    }

    fun updateDataset(photosList: List<Photo>) {
        this.photosList.addAll(photosList)
        notifyDataSetChanged()
    }
}