package com.vignesh.ubertest.repository.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Photos {

    @SerializedName("page")
    @Expose
    var page: Int = 0
    @SerializedName("pages")
    @Expose
    var pages: Int = 0
    @SerializedName("perpage")
    @Expose
    var perpage: Int = 0
    @SerializedName("total")
    @Expose
    var total: String? = null
    @SerializedName("photo")
    @Expose
    lateinit var photo: List<Photo>

}
