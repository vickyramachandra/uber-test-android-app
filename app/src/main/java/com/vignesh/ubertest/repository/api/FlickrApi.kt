package com.vignesh.ubertest.repository.api

import android.content.Context
import com.google.gson.Gson
import com.vignesh.ubertest.network.NetworkCallbacks
import com.vignesh.ubertest.network.NetworkManager
import com.vignesh.ubertest.repository.data.Photo
import com.vignesh.ubertest.repository.data.PhotosResponse
import io.reactivex.Observable

class FlickrApi {

    constructor (context: Context) {
        NetworkManager.initialize(context)
    }

    fun getPictures(searchTerm: String, pageNumber: Int = 1): Observable<List<Photo>> {
        return Observable.create({
            val url = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1&safe_search=1&text=$searchTerm&page=$pageNumber"
            NetworkManager.getRequest(url, object : NetworkCallbacks {
                override fun onSuccess(response: String) {
                    val photosResponse = Gson().fromJson<PhotosResponse>(response, PhotosResponse::class.java)
                    it.onNext(photosResponse.photos!!.photo)
                    it.onComplete()
                }

                override fun onError(exception: Exception) {
                    it.onError(exception)
                }
            })
        })
    }
}