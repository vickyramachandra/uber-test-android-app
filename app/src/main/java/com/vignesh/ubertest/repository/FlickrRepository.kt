package com.vignesh.ubertest.repository

import com.vignesh.ubertest.repository.api.FlickrApi
import com.vignesh.ubertest.repository.data.Photo
import io.reactivex.Observable

/**
 * @FlickrRepository - can decide if data has to be fetched from the api or db store
 * Note - We use only api for now
 */
class FlickrRepository(private val flickrApi: FlickrApi) {
    fun getPictures(searchTerm: String, pageNumber: Int): Observable<List<Photo>> {
        return flickrApi.getPictures(searchTerm, pageNumber)
    }
}