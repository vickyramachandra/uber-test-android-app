package com.vignesh.ubertest.repository.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PhotosResponse {
    @SerializedName("photos")
    @Expose
    var photos: Photos? = null
}