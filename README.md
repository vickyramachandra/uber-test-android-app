Android App for Uber's Mobile Coding Challenge

Please refer the file `project-structure.jpg` for a complete view of the project architecture

Download `app-debug.apk` and install on your Android device to run the app

These are the dependencies used in building the app

* `Volley` is used for n/w purposes - Can be replaced by `HttpUrlConnection`
* `RxJava2-RxAndroid` - Used for reactive programming and threading - Can be replaced by `AsyncTask`
* `Gson` - For json to model mapping - `org.json` package can be used for json parsing
* The app is completely written in `Kotlin`

Due to time constraints, these minor shortcuts were taken. As reasoned out above, replacing them should be straight forward

Here's a gist of the responsibilities which each layer in the app has been assigned to

* `manifests` - Contains the `AndroidManifest.xml`
* `application` - Contains the Application class named `UberChallenge`
* `network` - All n/w IO happens here
   * `NetworkCallbacks` - interface which delivers n/w response to API
   * `NetworkManager` - contains `volley` implementation of request queue for making n/w calls
* `repository` - Serves as the data provider. Only API implementation is given for now. Data store can also be added as a package within this
   * `FlickRepository` - Communicates with API for now (later can be given the reponsibility to decide to fetch from API or store)
   * `api` - Contains a class named `FlickApi` which communicates with n/w
   * `data` - Contains the data models which are formed by the API
* `view` - As the name says. All UI related code goes here
   * `activity` - Contains `PictureListActivity` which is the main and only activity
   * `adapter` - Contains `PhotoAdapter` to draw out the `GridView`
* `viewmodel` - view communicates with this package to get info from the repository
   * `FlickrListViewModel` - Provides access methods for UI population
   * `data` - Contains `PhotosList` which contains info for drawing the views